<!-- <img align= "center" src="https://manticore-labs.com/wp-content/uploads/2016/02/Footer-300x58.png" /> -->

# ProyectoPrueba

Este proyecto sirve para realizar un testing de los roles existentes en el manejo de proyectos con GitLab. Los roles existentes son los siguientes:
- [Usuario: Guest](#usuario-guest)
- [Usuario: Reporter](#usuario-reporter)
- [Usuario: Developer](#usuario-developer)
- [Usuario: Maintainer](#usuario-maintainer)

# Usuarios

### Usuario: Guest

* Es permitido la creacion de Issues con este usuario
* No es permitida la creacion de labels
* No es permitido creacion de milestone
* No es permitido la creacion y eliminacion de branches con este usuario
* Es permitido clonar el proyecto con este usuario
* No es permitido realizar commit y push al origen
* No es permitido la creacion de nuevos usuarios y asignacion de roles

### Usuario: Reporter

* Es permitido la creacion de Issues con este usuario
* Es permitida la creacion de labels
* Es permitido creacion de milestone
* No es permitido la creacion y eliminacion de branches con este usuario
* Es permitido clonar el proyecto con este usuario
* No es permitido realizar commit y push al origen
* No es permitido la creacion de nuevos usuarios y asignacion de roles

### Usuario: Developer

* Es permitido la creacion de Issues con este usuario
* Es permitida la creacion de labels
* Es permitido creacion de milestone
* Es permitido la creacion de branches con este usuario
* Es permitido eliminacion de cualquier branche que no sea la master con este usuario
* Es permitido clonar el proyecto con este usuario
* Es permitido realizar commit y push al origen
* No es permitido la creacion de nuevos usuarios y asignacion de roles

### Usuario: Maintainer

* Es permitido la creacion de Issues con este usuario
* Es permitida la creacion de labels
* Es permitido creacion de milestone
* Es permitido la creacion de branches con este usuario
* Es permitido eliminacion de cualquier branche que no sea la master con este usuario
* Es permitido clonar el proyecto con este usuario
* Es permitido realizar commit y push al origen
* Es permitido la creacion de nuevos usuarios y asignacion de roles